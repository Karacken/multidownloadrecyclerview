package com.suntec.oneread.downloadmanager;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.suntec.oneread.providers.DownloadManager;
import com.suntec.oneread.providers.downloads.DownloadService;
import com.suntec.oneread.utility.SDConstants;

import java.io.File;

public class DownloadHelper {

    public static final String ACTION_DIGIT_BOOK_DOWNLOAD = "action_digit_book_download";
    private static DownloadHelper ourInstance = new DownloadHelper();
    private DownloadManager mDm;
    private Context mContext;
    long download_id = -1;

    public static DownloadHelper getInstance() {
        return ourInstance;
    }

    private DownloadHelper() {
    }

    public void init(Context context) {
        mDm = new DownloadManager(context.getContentResolver(),
                context.getPackageName());

        mContext = context;
        startDownloadService();
    }

    public long startdownload(String bookname, String downloadurl) {
        try {
            File destfile = new File(SDConstants.APP_DIR + File.separator + bookname + ".epub");
            if (destfile.exists()) destfile.delete();
            DownloadManager.Request request = new DownloadManager.Request(
                    Uri.parse(downloadurl));
            System.out.println("test download 3 downloadurl = " + downloadurl);
            request.setShowRunningNotification(false);
            //    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
            download_id = mDm.enqueue((request).setDestinationInExternalPublicDir(SDConstants.APP_DIR_SHORT_PATH, bookname + ".epub"));
            return download_id;
        } catch (Exception e) {
            System.out.println("test download 4 error " + e.getMessage());
            e.printStackTrace();
        }
        return 0;
    }

    public void killdownload(long download_id) {
        mDm.remove(download_id);
    }


    public DownloadManager getDownloadManager() {
        return mDm;
    }


    public boolean getifPaused(long id) {
        return mDm.getifPaused(id);

    }

    public void pauseDownload(long id) {
        mDm.pauseDownload(id);

    }

    public void resumeDownload(long id) {
        mDm.resumeDownload(id);

    }

    public double getProgress(long download_id){
        double progress = -1.0;
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(download_id);

        Cursor c = mDm.query(query);
        try {
            if (c != null && c.moveToFirst()) {
                int sizeIndex = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
                int downloadedIndex = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);

                long size = c.getInt(sizeIndex);
                long downloaded = c.getInt(downloadedIndex);

                if (size != -1) {

                    progress = downloaded * 100.0 / size;
                }

            }
        } catch (Exception e) {

        } finally {
            c.close();
        }
        return progress;
    }

    public int getStatus(long download_id) {
        return mDm.getStatus(download_id);
    }

    private void startDownloadService() {
        Intent intent = new Intent();
        intent.setClass(mContext, DownloadService.class);
        mContext.startService(intent);
    }

}
