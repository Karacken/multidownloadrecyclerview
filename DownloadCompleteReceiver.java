package com.suntec.oneread.downloadmanager;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.suntec.oneread.R;
import com.suntec.oneread.activities.HomeActivity;
import com.suntec.oneread.database.SDDatabaseProvider;
import com.suntec.oneread.models.LibraryBook;
import com.suntec.oneread.providers.DownloadManager;
import com.suntec.oneread.utility.SDConstants;


public class DownloadCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            Log.d("DOWNLOADED_INFO", "inside");
            try {
                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(downloadId);
                Cursor c = DownloadHelper.getInstance().getDownloadManager().query(query);
                //String filePath = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    if (DownloadManager.STATUS_SUCCESSFUL == c
                            .getInt(columnIndex)) {
                        int bookid = 0;
                        try {

                            bookid = Integer.parseInt((c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE))
                                    .replace(".epub", "")));
                            SDDatabaseProvider.getInstance().setBookDownloaded(bookid);
                            LibraryBook b = SDDatabaseProvider.getInstance().getLibraryBookById(bookid);
//                            if(b!=null&& Prefs.getBoolean(context ,
//                                    NotificationSettingActivity.NOT_SETTING_SUCC_DOWNLOAD , true)) {
//                                createNotification(context , b);
//                            }

                            Intent singlespineintent = new Intent(context,SingleSpineService.class);
                            singlespineintent.setAction(DownloadHelper.ACTION_DIGIT_BOOK_DOWNLOAD);
                            singlespineintent.putExtra(SDConstants.EXTRA_BOOK_PRODUCT_ID, bookid);
                            context.startService(singlespineintent);
                          //  context.sendBroadcast(local_broadcast_intent);

                        } catch (Exception e) {
                        }
                        Log.d("DOWNLOADED_INFO", "Id=" + bookid);
                    }
                    c.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void createNotification(Context context, LibraryBook b) {

        Intent intent = new Intent(context, HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("FROM_NOTIFICATION", 1);
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle("One Read")
                .setContentText(b.getTitle() + " is downloaded successfully!\n")
                .setContentIntent(pIntent).setSmallIcon(R.drawable.one_read_launcher)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(b.getId(), builder.build());

    }


}
