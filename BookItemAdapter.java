package com.suntec.oneread.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.suntec.oneread.R;
import com.suntec.oneread.customviews.DownloadBookDialog;
import com.suntec.oneread.downloadmanager.DownloadHelper;
import com.suntec.oneread.interfaces.CollectionMenuListener;
import com.suntec.oneread.interfaces.OnActionListener;
import com.suntec.oneread.models.LibraryBook;
import com.suntec.oneread.popups.ContextMenuCollection;
import com.suntec.oneread.utility.EpubUtils;
import com.suntec.oneread.utility.SDConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookItemAdapter extends RecyclerView.Adapter<BookItemAdapter.CatViewHolder> {
    private List<LibraryBook> datalist = new ArrayList<>();
    private List<LibraryBook> originallist = new ArrayList<>();
    private List<Integer> selected_pos = new ArrayList<>();
    private boolean is_selection_on = false;
    private OnActionListener mActionListener;
    private SDConstants.LAYOUTTYPE mLayoutType = SDConstants.LAYOUTTYPE.GRID;
    private SparseIntArray bookid_mapping = new SparseIntArray();
    private DownloadBookDialog downloadBookDialog;
    private Activity mActivity;
    private BookFilter bookFilter;
    private Handler mHandler = new Handler();
    private final HashMap<Integer, Boolean> downloadingItems = new HashMap<>();
    private CollectionMenuListener menuListener;
    private Timer downloadprogresstimer;
    private RecyclerView.LayoutManager layoutManager;

    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public BookItemAdapter(Activity activity, List<LibraryBook> datalist, OnActionListener listener, CollectionMenuListener menuListener, SDConstants.LAYOUTTYPE layouttype) {
        downloadprogresstimer = new Timer();
        this.selected_pos.clear();
        this.datalist = new ArrayList<>();
        this.datalist.clear();
        originallist = datalist;
        this.datalist.addAll(datalist);
        this.mActionListener = listener;
        this.menuListener = menuListener;

        initbookmapping();
        initDownloadingItemList();
        scheduletimer();
        mLayoutType = layouttype;
        downloadBookDialog = new DownloadBookDialog(activity);
        mActivity = activity;

    }

    private void initDownloadingItemList() {
        for (LibraryBook book : datalist) {
            if (book.getDownloadid() != 0 && book.getIsdownloaded() == 0
                    && DownloadHelper.getInstance().getProgress(book.getDownloadid()) != 100
                    ) {
                synchronized (downloadingItems) {
                    downloadingItems.put(book.getProductId(), !DownloadHelper.getInstance().getifPaused(book.getDownloadid()));
                }
            }
        }
    }

    private void scheduletimer() {
        downloadprogresstimer.scheduleAtFixedRate(downloadtimertask, 0, 500);
    }


    public void removeItem(LibraryBook book) {
        int position = datalist.indexOf(book);
        if (position >= 0) {
            datalist.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, datalist.size());
        }

    }

    public boolean addItem(LibraryBook model) {
        if (!datalist.contains(model)) {
            model.setIsLocked(0);
            datalist.add(model);
            notifyItemInserted(datalist.size() - 1);
            return true;
        }
        return false;
    }

    public void showDownload(final int position) {
        if (datalist.get(position).getIsdownloaded() == 1) {
            EpubUtils.getInstance().openBook(SDConstants.APP_DIR + datalist.get(position).getProductId() + ".epub",
                    mActivity, datalist.get(position));
        } else {
            downloadBookDialog.setDownloadClickListener(new DownloadBookDialog.DownloadClickListener() {
                @Override
                public void OnDownloadStarted() {
                    downloadingItems.put(datalist.get(position).getProductId(), true);
                    notifyItemChanged(position);
                }

                @Override
                public void OnDownloadPaused() {
                    downloadingItems.put(datalist.get(position).getProductId(), false);
                    notifyItemChanged(position);
                }

                @Override
                public void OnDownloadResumed() {
                    downloadingItems.put(datalist.get(position).getProductId(), true);
                    notifyItemChanged(position);
                }
            });
            downloadBookDialog.show(datalist.get(position));
        }
    }

    public void setDatalist(List<LibraryBook> datalist) {
        this.selected_pos.clear();
        this.datalist.clear();
        this.datalist.addAll(datalist);
        initDownloadingItemList();
        initbookmapping();
        notifyDataSetChanged();
    }

    @Override
    public CatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (mLayoutType == SDConstants.LAYOUTTYPE.GRID) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dlg_bk_slct_item, parent, false);
        } else if (mLayoutType == SDConstants.LAYOUTTYPE.HORIZON) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dlg_bk_slct_item_horizon, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dlg_bk_slct_item_big, parent, false);
        }
        return new CatViewHolder(itemView);
    }

    public void clearSelection() {
        selected_pos.clear();
        notifyDataSetChanged();
    }

    private void refreshSelectedItems(List<Integer> selected_pos) {
        for (int pos : selected_pos) {
            notifyItemChanged(pos);
        }
    }

    @Override
    public void onBindViewHolder(CatViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return this.datalist.size();
    }


    public class CatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private int position = 0;
        @BindView(R.id.imgBook)
        public ImageView imgBook;

        @BindView(R.id.img_downoad_icon)
        public ImageView img_downoad_icon;

        @BindView(R.id.imgMenu)
        public LinearLayout imgMenu;

        @BindView(R.id.book_selector_view)
        public View book_selector_view;

        @BindView(R.id.book_lock)
        public ImageView book_lock;

        @BindView(R.id.txtBooktitle)
        public TextView txtBooktitle;

        @BindView(R.id.txtBookInfo)
        public TextView txtBookInfo;

        @BindView(R.id.download_active_bg)
        public View download_active_bg;

        @BindView(R.id.download_progress_bar)
        public CircleProgress download_progress_bar;

        @BindView(R.id.paused_icon)
        public View paused_icon;

        public CatViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            download_progress_bar.setMax(100);
            imgBook.setLongClickable(true);
            imgBook.setOnClickListener(this);

            MaterialRippleLayout.on(imgMenu)
                    .rippleColor(Color.BLACK)
                    .create();
            if (menuListener == null) {
                imgMenu.setVisibility(View.GONE);
            } else {
                imgBook.setOnLongClickListener(this);
            }

        }


        @Override
        public boolean onLongClick(View v) {
            setIs_selection_on(true);
            if (!selected_pos.contains(position)) {
                selected_pos.add(position);
            } else {
                selected_pos.remove(Integer.valueOf(position));
            }
            selectunselect(position);
            return true;
        }

        @Override
        public void onClick(View v) {
            if (is_selection_on) {
                if (!selected_pos.contains(position)) {
                    selected_pos.add(position);
                } else {
                    selected_pos.remove(Integer.valueOf(position));
                }
                selectunselect(position);
            } else {
                showDownload(position);
            }
        }


        public void bind(final int position) {
            final LibraryBook book = datalist.get(position);
            this.position = position;
            imgBook.setImageResource(R.drawable.img_placeholder);

            ImageLoader.getInstance().displayImage(book.getCoverImageUrl(), imgBook);
            book_lock.setVisibility
                    (book.getIsLocked() == 1 ? View.VISIBLE : View.GONE);
            img_downoad_icon.setVisibility(View.VISIBLE);
            img_downoad_icon.setImageResource
                    (book.getIsdownloaded() == 1 ?
                            R.drawable.ic_done_24dp :
                            R.drawable.ic_file_download_24dp);
            selectunselect(position);
            if (menuListener != null) {
                imgMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new ContextMenuCollection(mActivity, imgMenu, position, datalist.get(position), menuListener);
                    }
                });
            }
            txtBooktitle.setText(datalist.get(position).getTitle());
            String desc = datalist.get(position).getDesciption();
            if (desc != null)
                desc = desc.trim();
            else
                desc = "";
            txtBookInfo.setText(Html.fromHtml(desc));

            synchronized (downloadingItems) {
                boolean isdownloading = downloadingItems.containsKey(datalist.get(position).getProductId());
                boolean isrunning = false;
                if (isdownloading)
                    isrunning = downloadingItems.get(datalist.get(position).getProductId());
                if (isdownloading && isrunning) {
                    download_progress_bar.setVisibility(View.VISIBLE);
                    download_active_bg.setVisibility(View.VISIBLE);
                    paused_icon.setVisibility(View.GONE);
                } else if (isdownloading) {
                    download_progress_bar.setVisibility(View.GONE);
                    download_active_bg.setVisibility(View.VISIBLE);
                    paused_icon.setVisibility(View.VISIBLE);
                } else {
                    download_progress_bar.setVisibility(View.GONE);
                    download_active_bg.setVisibility(View.GONE);
                    paused_icon.setVisibility(View.GONE);
                }

            }
            download_progress_bar.setProgress(book.getProgress());
            datalist.get(position).setProgressbar(download_progress_bar);
            datalist.get(position).setDownload_active_bg(download_active_bg);
        }

        private void selectunselect(int position) {
            if (selected_pos.contains(position)) {
                book_selector_view.setActivated(true);
                imgMenu.setClickable(false);
            } else {
                book_selector_view.setActivated(false);
                imgMenu.setClickable(true);
            }
        }
    }

    public List<LibraryBook> getSelectedBooks() {
        List<LibraryBook> selected_books = new ArrayList<>();
        for (int pos : selected_pos) {
            selected_books.add(datalist.get(pos));
        }
        return selected_books;
    }


    public void toggleSelection(boolean isactive, boolean dorefresh) {
        List<Integer> x_selectedpos = new ArrayList<>();
        x_selectedpos.addAll(selected_pos);
        if (is_selection_on)
            clearSelection();
        if (dorefresh)
            refreshSelectedItems(x_selectedpos);
        setIs_selection_on(isactive);
    }

    public void toggleSelectedItem(int position) {
        if (selected_pos.contains(position))
            selected_pos.remove(position);
        else
            selected_pos.add(position);
        toggleSelection(true, false);
        notifyItemChanged(position);
    }

    public void toggleSelectAll() {
        for (int i = 0; i < datalist.size(); i++) {
            if (!selected_pos.contains(i))
                selected_pos.add(i);

        }
        toggleSelection(true, true);
    }

    public SDConstants.LAYOUTTYPE getmLayoutType() {
        return mLayoutType;
    }

    public void setmLayoutType(SDConstants.LAYOUTTYPE mLayoutType) {
        this.mLayoutType = mLayoutType;
    }

    public void sort(SDConstants.SORT_TYPE sort_type) {
        FilterComparator comparator = new FilterComparator();
        comparator.setSort_type(sort_type);
        Collections.sort(datalist, comparator);
        notifyDataSetChanged();
    }

    private class FilterComparator<T> implements Comparator<T> {

        public void setSort_type(SDConstants.SORT_TYPE sort_type) {
            this.sort_type = sort_type;
        }

        private SDConstants.SORT_TYPE sort_type = SDConstants.SORT_TYPE.TITLE_ATOZ;

        @Override
        public int compare(T lhs, T rhs) {
            switch (sort_type) {
                case TITLE_ATOZ:
                    return ((LibraryBook) lhs).getTitle().trim().compareToIgnoreCase(((LibraryBook) rhs).getTitle().trim());

                case TITLE_ZTOA:
                    return ((LibraryBook) rhs).getTitle().trim().compareToIgnoreCase(((LibraryBook) lhs).getTitle().trim());

                case AUTHOR_ATOZ:
                    return ((LibraryBook) lhs).getTitle().trim().compareToIgnoreCase(((LibraryBook) rhs).getTitle().trim());

                case AUTHOR_ZTOA:
                    return ((LibraryBook) rhs).getTitle().trim().compareToIgnoreCase(((LibraryBook) lhs).getTitle().trim());

                default:
                    return 0;
            }
        }

        @Override
        public boolean equals(Object object) {
            return false;
        }
    }

    public void resetSearch() {
        this.datalist.clear();
        this.datalist.addAll(originallist);
        this.selected_pos.clear();
        notifyDataSetChanged();
    }

    private class BookFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                results.values = originallist;
                results.count = originallist.size();
            } else {
                constraint = constraint.toString().toUpperCase();
                List<LibraryBook> mBooklist = new ArrayList<>();

                for (LibraryBook p : originallist) {
                    if (p.getTitle().toUpperCase().contains(constraint)
                            || p.getDesciption().toUpperCase().contains(constraint))
                        mBooklist.add(p);
                }
                results.values = mBooklist;
                results.count = mBooklist.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            datalist = (List<LibraryBook>) results.values;
            notifyDataSetChanged();
        }

    }

    public Filter getFilter() {
        if (bookFilter == null)
            bookFilter = new BookFilter();
        return bookFilter;
    }


    private TimerTask downloadtimertask = new TimerTask() {
        @Override
        public void run() {
            synchronized (downloadingItems) {
                List<Integer> downloadingItems_copy = new ArrayList<>(downloadingItems.keySet());
                for (final int i : downloadingItems_copy) {
                    final Integer index = bookid_mapping.get(i);
                    if (datalist.size() > index) {
                        final CircleProgress progressbar = datalist.get(index).getProgressbar();
                        final int progress = (int) DownloadHelper.getInstance().getProgress(datalist.get(index).getDownloadid());

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (progressbar != null && isItemVisible(index)) {
                                    if (progress == 100) {
                                        datalist.get(index).setIsdownloaded(1);
                                        downloadingItems.remove(i);
                                        notifyItemChanged(index);
                                    } else {
                                        if (progress < 0)
                                            progressbar.setProgress(0);
                                        else
                                            progressbar.setProgress(progress);
                                        progressbar.invalidate();
                                    }
                                }
                            }
                        });
                    }
                }
            }
        }
    };

    private void initbookmapping() {
        bookid_mapping.clear();
        for (int i = 0; i < datalist.size(); i++) {
            bookid_mapping.put(datalist.get(i).getProductId(), i);

        }
    }

    private void setIs_selection_on(boolean is_selection_on) {
        if (mActionListener != null) mActionListener.selectionToggle(is_selection_on);
        this.is_selection_on = is_selection_on;
    }


    private boolean isItemVisible(int position) {
        return layoutManager != null && ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition() <= position &&
                ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition() >= position;
    }
}
