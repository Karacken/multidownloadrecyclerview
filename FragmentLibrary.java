package com.suntec.oneread.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionButton;
import com.google.gson.JsonObject;
import com.suntec.oneread.R;
import com.suntec.oneread.activities.HomeActivity;
import com.suntec.oneread.adapter.BookItemAdapter;
import com.suntec.oneread.customviews.DiPopupFilter;
import com.suntec.oneread.database.SDDatabaseProvider;
import com.suntec.oneread.interfaces.LibraryDownloadListener;
import com.suntec.oneread.models.AbstractResponseList;
import com.suntec.oneread.models.LibraryBook;
import com.suntec.oneread.models.ResponseMeta;
import com.suntec.oneread.utility.GridItemDecoration;
import com.suntec.oneread.utility.LinearItemDecoration;
import com.suntec.oneread.utility.RetrofitHelper;
import com.suntec.oneread.utility.SDConstants;
import com.suntec.oneread.utility.SDSingleton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentLibrary extends Fragment
        implements DiPopupFilter.FilterCallback, SearchView.OnQueryTextListener,
        LibraryDownloadListener {

    private BookItemAdapter mLibraryAdapter;
    private List<LibraryBook> datalist = new ArrayList<>();
    private boolean isdownloading = false;
    private GridLayoutManager gridLayoutManager;
    private GridLayoutManager linearLayoutManager;
    private SDConstants.LAYOUTTYPE mLayouttype = SDConstants.LAYOUTTYPE.GRID;
    private MenuItem itemSearch;
    private MenuItem itemFilter;

    GridItemDecoration gridItemDecoration;
    LinearItemDecoration linearItemDecoration;
    @BindView(R.id.gridformation)
    FloatingActionButton gridformation;
    @BindView(R.id.coll_frag_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_library, menu);
        itemSearch = menu.findItem(R.id.do_search);
        itemFilter = menu.findItem(R.id.do_sort);
        if (datalist.size() > 0) {
            itemSearch.setVisible(true);
            itemFilter.setVisible(true);
        } else {
            itemSearch.setVisible(false);
            itemFilter.setVisible(false);
        }
        ((HomeActivity) getActivity()).bindSearchView(itemSearch, librarySearchListener);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.do_sort) {
            DiPopupFilter.show(getActivity(), getView(), this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.out.println("onCreateView " + this.getClass().getName());
        View view = inflater.inflate(R.layout.fragement_library, container, false);
        getActivity().setTitle("My library");
        ((HomeActivity) getActivity()).showToolBar(true);
        ButterKnife.bind(this, view);
        initRecycler();
        initControls();

        return view;
    }

    private void initControls() {

        gridformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gridformation.isSelected()) {
                    gridformation.setSelected(false);
                    mLayouttype = SDConstants.LAYOUTTYPE.GRID;

                    toggleLayout();
                } else {
                    gridformation.setSelected(true);
                    mLayouttype = SDConstants.LAYOUTTYPE.LINEAR;
                    toggleLayout();
                }
            }
        });
    }

    private void toggleLayout() {
        switch (mLayouttype) {
            case GRID:
                mRecycler.setLayoutManager(gridLayoutManager);
                mLibraryAdapter.setLayoutManager(gridLayoutManager);
                mRecycler.removeItemDecoration(linearItemDecoration);
                mRecycler.addItemDecoration(gridItemDecoration);
                break;
            case LINEAR:
                mRecycler.setLayoutManager(linearLayoutManager);
                mLibraryAdapter.setLayoutManager(linearLayoutManager);
                mRecycler.removeItemDecoration(gridItemDecoration);
                mRecycler.addItemDecoration(linearItemDecoration);

                break;
        }
        mLibraryAdapter.setmLayoutType(mLayouttype);
        mRecycler.setAdapter(mLibraryAdapter);
    }

    private void initRecycler() {
        loadBooks(SDDatabaseProvider.getInstance().getLibraryBooks());
        gridLayoutManager = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager = new GridLayoutManager(getActivity(), 1, LinearLayoutManager.VERTICAL, false);
        gridItemDecoration = new GridItemDecoration(getActivity().getResources().getDimensionPixelSize(R.dimen._4sdp));
        linearItemDecoration = new LinearItemDecoration(getResources().getDimensionPixelSize(R.dimen._8sdp));
        mRecycler.setLayoutManager(gridLayoutManager);
        mRecycler.addItemDecoration(gridItemDecoration);
        mLibraryAdapter = new BookItemAdapter(getActivity(), datalist, null, null , SDConstants.LAYOUTTYPE.GRID);
        mLibraryAdapter.setLayoutManager(gridLayoutManager);
        mRecycler.setAdapter(mLibraryAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLibraryBooksfromWeb();
            }
        });
    }


    public List<LibraryBook> getDatalist() {
        return datalist;
    }

    public void setDatalist(List<LibraryBook> datalist) {
        this.datalist = datalist;
    }


    @Override
    public void showDownload(int position) {
        mLibraryAdapter.showDownload(position);
    }

    @Override
    public void isDownloading(boolean status) {
        isdownloading = status;
        mSwipeRefreshLayout.setEnabled(!status);
    }


    private void loadBooks(List<LibraryBook> datalist) {
        if (datalist.size() > 0) {
            this.datalist.clear();
            this.datalist.addAll(datalist);
            if (itemFilter != null && itemSearch != null) {
                itemSearch.setVisible(true);
                itemFilter.setVisible(true);
            }
        } else {
            getLibraryBooksfromWeb();
        }

    }

    @Override
    public void sort(SDConstants.SORT_TYPE type) {
        mLibraryAdapter.sort(type);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    public interface LibrarySearchListener {

        void onSearchOpen();

        void onSearch(String qry);

        void onQueryText(String qry);

        void onSearchClose();
    }


    private LibrarySearchListener librarySearchListener = new LibrarySearchListener() {
        @Override
        public void onSearchOpen() {
            mSwipeRefreshLayout.setEnabled(false);
        }

        @Override
        public void onSearch(String qry) {
//            mLibraryAdapter.getFilter().filter(qry);
        }

        @Override
        public void onQueryText(String qry) {
            mLibraryAdapter.getFilter().filter(qry);
        }

        @Override
        public void onSearchClose() {
            mLibraryAdapter.getFilter().filter("");

            if (!isdownloading)
                mSwipeRefreshLayout.setEnabled(true);

        }
    };

    private Callback<AbstractResponseList<ResponseMeta, LibraryBook>> librarycallback = new Callback<AbstractResponseList<ResponseMeta, LibraryBook>>() {
        @Override
        public void onResponse(Call<AbstractResponseList<ResponseMeta, LibraryBook>> call, Response<AbstractResponseList<ResponseMeta, LibraryBook>> response) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (response != null && response.body() != null && response.body().getResponseMeta().getCode() == 200) {
                if (response.body().getData() != null && response.body().getData().size() != 0) {
                    if (itemFilter != null && itemSearch != null) {
                        itemSearch.setVisible(true);
                        itemFilter.setVisible(true);
                    }
                    SDDatabaseProvider.getInstance().addLibraryBooks(response.body().getData());
                    datalist = SDDatabaseProvider.getInstance().getLibraryBooks();
                    mLibraryAdapter.setDatalist(datalist);
                }
            } else {
                showInternetDialogAlert();
            }
        }

        @Override
        public void onFailure(Call<AbstractResponseList<ResponseMeta, LibraryBook>> call, Throwable t) {
            showInternetDialogAlert();
        }

    };

    private void showInternetDialogAlert() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void serviceExceptionHandler() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void getLibraryBooksfromWeb() {
        int customer_id = SDSingleton.getInstance().loginData.get(0).getId();
        JsonObject request = new JsonObject();
        request.addProperty("customerId", customer_id);
        request.addProperty("storeId", SDConstants.STORE_ID);
        System.out.println("test cust library " + request.toString());
        RetrofitHelper.getInstance().callgetLibraryBooksService(librarycallback, request);
    }
}
